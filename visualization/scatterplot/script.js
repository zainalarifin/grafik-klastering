var ScatterPlot = function () {
        return {
            initChart: function(parentWidth, containerID) {
                var margin = { top: 20, right: 20, bottom: 30, left: 40 },
                    width = +parentWidth - margin.left - margin.right,
                    height = 900 - margin.top - margin.bottom;

                var x = d3.time.scale()
                        .domain([ new Date(2015, 0, 1), new Date(2015, 11, 30)])
                        .range([0, width]);

                var y = d3.scale.linear()
                        .range([height, 0]);
                
                var rx = d3.scale.log()
                        .base(Math.E)
                        //.domain([ Math.exp(0), Math.exp(9)])
                        .range([2,15]);

                var xAxis = d3.svg.axis()
                        .scale(x)
                        .ticks(d3.time.months)
                        .tickSize(16,0)
                        .tickFormat(d3.time.format("%B"))
                        .orient("bottom");

                var yAxis = d3.svg.axis()
                        .scale(y)
                        .orient("left");

                var svg = d3.select(containerID).append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    d3.json("../json/tuna_catch.json", function(error, result){
                        var data = _.map( result.data, function(d){
                                d.month_of_catch = new Date(2015, (d.month - 1) , 15);
                                return d;
                        });
                        console.log(data[0]);
                        //x.domain(d3.extent(data, function(d) { return d.month_of_catch; }));
                        y.domain(d3.extent(data, function(d) { return d.weight })).nice();
                        rx.domain(d3.extent(data, function(d) { return Math.exp( d.fork_length ); })).nice();
                                
                        svg.append("g")
                            .attr("class", "x axis")
                            .attr("transform", "translate(0," + height + ")")
                            .call(xAxis)
                        .append("text")
                            .attr("class", "label")
                            .attr("x", width)
                            .attr("y", -6)
                            .style("text-anchor", "end")
                            .text("Month of catch");

                        svg.append("g")
                            .attr("class", "y axis")
                            .call(yAxis)
                        .append("text")
                            .attr("class", "label")
                            .attr("transform", "rotate(-90)")
                            .attr("y", 6)
                            .attr("dy", ".71em")
                            .style("text-anchor", "end")
                            .text("SBT Weight (kg)");

                        svg.selectAll(".dot")
                            .data(data)
                        .enter().append("ellipse")
                            .attr("class", "dot")
                            .attr("rx", function(d) { return rx( Math.exp( d.fork_length) ); })
                            .attr("ry", 3.5)
                            .attr("cx", function(d) { return x(d.month_of_catch); })
                            .attr("cy", function(d) { return y(d.weight); })
                            .style("fill", '#000');

                    });
            }
        };
    }();

$(document).ready(function(){
    var container_id = "#scatterplot";
    var parent_width = $(container_id).parent().width();
    ScatterPlot.initChart(parent_width, container_id);
});
